// TODO: 用户名称需修改为自己的名称
var userName = 'Lu仔酱';
// 朋友圈页面的数据
var data = [{
  user: {
    name: '阳和', 
    avatar: './img/avatar2.png'
  }, 
  content: {
    type: 0, // 多图片消息
    text: '华仔真棒，新的一年继续努力！',
    pics: ['./img/reward1.png', './img/reward2.png', './img/reward3.png', './img/reward4.png'],
    share: {},
    timeString: '3分钟前'
  }, 
  reply: {
    hasLiked: false,
    likes: ['Guo封面', '源小神'],
    comments: [{
      author: 'Guo封面',
      text: '你也喜欢华仔哈！！！'
    },{
      author: '喵仔zsy',
      text: '华仔实至名归哈'
    }]
  }
}, {
  user: {
    name: '伟科大人',
    avatar: './img/avatar3.png'
  },
  content: {
    type: 1, // 分享消息
    text: '全面读书日',
    pics: [],
    share: {
      pic: 'http://coding.imweb.io/img/p3/transition-hover.jpg',
      text: '飘洋过海来看你'
    },
    timeString: '50分钟前'
  },
  reply: {
    hasLiked: false,
    likes: ['阳和'],
    comments: []
  }
}, {
  user: {
    name: '深圳周润发',
    avatar: './img/avatar4.png'
  },
  content: {
    type: 2, // 单图片消息
    text: '很好的色彩',
    pics: ['http://coding.imweb.io/img/default/k-2.jpg'],
    share: {},
    timeString: '一小时前'
  },
  reply: {
    hasLiked: false,
    likes:[],
    comments: []
  }
}, {
  user: {
    name: '喵仔zsy',
    avatar: './img/avatar5.png'
  },
  content: {
    type: 3, // 无图片消息
    text: '以后咖啡豆不敢浪费了',
    pics: [],
    share: {},
    timeString: '2个小时前'
  }, 
  reply: {
    hasLiked: false,
    likes:[],
    comments: []
  }
}];

// 相关 DOM
var $page = $('.page-moments');
var $momentsList = $('.moments-list');

/**
 * 点赞内容 HTML 模板
 * @param {Array} likes 点赞人列表
 * @return {String} 返回html字符串
 */
function likesHtmlTpl(likes) {
  if (!likes.length) {
    return '';
  }
  var  htmlText = ['<div class="reply-like"><i class="icon-like-blue"></i>'];
  // 点赞人的html列表
  var likesHtmlArr = [];
  // 遍历生成
  for(var i = 0, len = likes.length; i < len; i++) {
    likesHtmlArr.push('<a class="reply-who" href="#">' + likes[i] + '</a>');
  }
  // 每个点赞人以逗号加一个空格来相隔
  var likesHtmlText = likesHtmlArr.join(', ');
  htmlText.push(likesHtmlText);
  htmlText.push('</div>');
  return htmlText.join('');
}
/**
 * 评论内容 HTML 模板
 * @param {Array} likes 点赞人列表
 * @return {String} 返回html字符串
 */
function commentsHtmlTpl(comments) {
  if (!comments.length) {
    return '';
  }
  var  htmlText = ['<div class="reply-comment">'];
  for(var i = 0, len = comments.length; i < len; i++) {
    var comment = comments[i];
    htmlText.push('<div class="comment-item"><a class="reply-who" href="#">' + comment.author + '</a>：' + comment.text + '</div>');
  }
  htmlText.push('</div>');
  return htmlText.join('');
}
/**
 * 评论点赞总体内容 HTML 模板
 * @param {Object} replyData 消息的评论点赞数据
 * @return {String} 返回html字符串
 */
function replyTpl(replyData) {
  var htmlText = [];
  htmlText.push('<div class="reply-zone">');
  htmlText.push(likesHtmlTpl(replyData.likes));
  htmlText.push(commentsHtmlTpl(replyData.comments));
  htmlText.push('</div>');
  return htmlText.join('');
}
/**
 * 多张图片消息模版 （可参考message.html）
 * @param {Object} pics 多图片消息的图片列表
 * @return {String} 返回html字符串
 */
function multiplePicTpl(pics) {
  var htmlText = [];
  htmlText.push('<ul class="item-pic">');
  for (var i = 0, len = pics.length; i < len; i++) {
    htmlText.push('<img class="pic-item it-img" src="' + pics[i] + '">')
  }
  htmlText.push('</ul>');
  return htmlText.join('');
}
function shareMessageTpl(pic,txt) {
  var htmlText = [];
  htmlText.push('<p class="item-share">');
  htmlText.push('<img class="pic-item" src="' + pic + '">');
  htmlText.push(txt);
  htmlText.push('</p>');
  return htmlText.join('');
}
function onlyImgTpl(pic) {
  var htmlText = [];
  htmlText.push('<img class="item-only-img it-img" src="' + pic + '">');
  return htmlText.join('');
}

/**
 * 循环：消息体 
 * @param {Object} messageData 对象
 */ 
function messageTpl(messageData) {
  var user = messageData.user;
  var content = messageData.content;
  var htmlText = [];
  htmlText.push('<div class="moments-item" data-index="0">');
  // 消息用户头像
  htmlText.push('<a class="item-left" href="#">');
  htmlText.push('<img src="' + user.avatar + '" width="42" height="42" alt=""/>');
  htmlText.push('</a>');
  // 消息右边内容
  htmlText.push('<div class="item-right">');
  // 消息内容-用户名称
  htmlText.push('<a href="#" class="item-name">' + user.name + '</a>');
  // 消息内容-文本信息
  htmlText.push('<p class="item-msg">' + content.text + '</p>');
  // 消息内容-图片列表 
  var contentHtml = '';
  // 目前只支持多图片消息，需要补充完成其余三种消息展示
  switch(content.type) {
      // 多图片消息
    case 0:
      contentHtml = multiplePicTpl(content.pics);
      break;
    case 1:
      // TODO: 实现分享消息
      contentHtml = shareMessageTpl(content.share.pic,content.share.text);
      break;
    case 2:
      // TODO: 实现单张图片消息
      contentHtml = onlyImgTpl(content.pics);
      break;
    case 3:
      // TODO: 实现无图片消息
  }
  htmlText.push(contentHtml);
  // 消息时间和回复按钮
  htmlText.push('<div class="item-ft">');
  htmlText.push('<span class="item-time">' + content.timeString + '</span>');
  htmlText.push('<div class="item-reply-btn">');
  htmlText.push('<span class="item-reply"></span>');
  htmlText.push('</div></div>');
  // 消息回复模块（点赞和评论）
  htmlText.push(replyTpl(messageData.reply));
  htmlText.push('</div></div>');
  return htmlText.join('');
}





/**
 * 页面渲染函数：render
 */
function render() {
  var messageHtml = '';
  // TODO: 目前只渲染了一个消息（多图片信息）,需要展示data数组中的所有消息数据。
  for(var i = 0; i < 4; i++) {
    messageHtml += messageTpl(data[i]);
  }
  $momentsList.html(messageHtml);
  $momentsList.append('<div class="bwrap"> <div class="right-btn"><div class="like"><i class="like-i"></i><span>点赞</span></div><div class="comment"><i class="comment-i"></i><span>评论</span></div></div></div>')
}
//点击回复按钮菜单栏伸展收缩
var self;//存放当前目标
var name;
var parent;
function itemAni() { 
  //事件委托
  $momentsList.on('click', '.item-reply',function(e) {
    if(self !== undefined){
      if( e.target !== self) {
        $('.bwrap').css({width:0});
      }
    }
    self = e.target;//存放当前目标
    name = $(self).parents('.item-right').children('.item-name').text()//获取该用户名称
    parent = $(self).parents('.item-right')
    //根据数据判定点赞情况
    data.forEach((item) => {
      if(item.user.name === name){
        var is = item.reply.likes.indexOf(userName)
        if(is > -1) {
          $('.like span').text('取消')
        }else{
          $('.like span').text('点赞')
        }
      }
    })
    //获取按钮位置信息
    var position = $(this).offset();
    $('.bwrap').offset({
      top: position.top - 14 
    })
    //获取点赞评论菜单宽度
    var wid = $('.bwrap').outerWidth();
    if( wid === 200) {
      $('.bwrap').animate({width:0})
    }else{
      $('.bwrap').animate({width:"200px"})
    }
  })
  //监听页面,点赞评论按钮站展开时，点击其他地方，菜单栏收回
  $page.on('click',function(e){
    if(self !== undefined){
      if( e.target !== self) {
        $('.bwrap').css({width:0});
      }
    }
  })
}
//点赞事件
function clickLike() {
  $('.like span').on('click',function() {
    var name = $(self).parents('.item-right').children('.item-name').text()//保存被点赞用户的名字
    var parent = $(self).parents('.item-right')
    var like = $(this).text();//获取是否点赞
    if(like === '点赞') {
      data.forEach((item)=>{
        if(item.user.name === name){
          item.reply.likes.push(userName)
          if(parent.find('.reply-zone').children().length>0) {
            parent.find('.reply-like').replaceWith(likesHtmlTpl(item.reply.likes))
          }else{
            parent.find('.reply-zone').append(likesHtmlTpl(item.reply.likes))
          }
          $(this).text('取消')
        }
      })
    }else{
      data.forEach((item)=>{
        if(item.user.name === name){
          item.reply.likes.pop()
          if(item.reply.likes.length>1) {
            parent.find('.reply-like').replaceWith(likesHtmlTpl(item.reply.likes))
          }else{
            parent.find('.reply-like').remove()
          }
          $(this).text('点赞')
        }
      })
    }
  })
}


//增加评论
function addComment() {
  $('.comment').on('click',function(){
    $('.comTpl').show()
    $('.comTpl input').attr("autofocus","autofocus")
    $('.bwrap').animate({width:0});
    return false
  })
  $('.comTpl input').on('keydown',function(){
    let commsg = $(this).val().trim()   
    if(commsg !== ''){
      $('.comTpl button').attr("disabled",false);
    }
  })
  $('.comTpl button').on('click',function(){
    let commsg =  $('.comTpl input').val().trim()
    data.forEach((item)=>{
      if(item.user.name === name){
        item.reply.comments.push({
          author: userName,
          text: commsg
        })
        if(parent.find('.reply-comment').length>0) {  
          parent.find('.reply-comment').replaceWith(commentsHtmlTpl(item.reply.comments))
        }else{
          parent.find('.reply-zone').append(commentsHtmlTpl(item.reply.comments))
        }
      }
    })
    $('.comTpl input').val("")
    $('.comTpl').hide()
  })
  $page.on('click',function(e){
    var display =$('comTpl').css('display');
    if(e.screenY < 700){
      $('.comTpl').hide()
    }
  })
}

function imgLarge(){
  let _this;
  $('.it-img').on('click',function(){
    _this = this;
    $(this).removeClass('pic-item');
    $('.overlay').show()
    $(this).addClass('itImg')
    return false
  })
  if($('.overlay').attr('display') !== 'none'){
    $page.on('click',function(){
      $(_this).removeClass('itImg');
    $('.overlay').hide()
    $(_this).addClass('pic-item')
    })
  }
}

/**
 * 页面绑定事件函数：bindEvent
 */
function bindEvent() {
  itemAni()
  clickLike()
  addComment()
  imgLarge()
}


// $page.on('click',function(e) {
  
//   if( e.target.className !== 'item-reply'){
//     $('.bwrap').hide();
//   }
  
  
// })

/**
 * 页面入口函数：init
 * 1、根据数据页面内容
 * 2、绑定事件
 */
function init() {
  // 渲染页面
  render();
  bindEvent();
}

init();